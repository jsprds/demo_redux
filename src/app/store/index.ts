import { combineReducers } from 'redux';

import { counterReducer, CounterStateInterface, counterInitialState } from './counter';

export interface AppState {
    counter: CounterStateInterface,
};

export const rootReducer = combineReducers<AppState>({
    counter: counterReducer
});

export const initialState: AppState = {
    counter: counterInitialState
};

export * from './store.module';
