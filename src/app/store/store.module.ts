import { NgModule } from '@angular/core';
import { NgReduxModule } from '@angular-redux/store';

import { CounterActions } from './counter';

@NgModule({
    imports: [
        NgReduxModule
    ],
    providers: [
        // list your injectable services (like actioncreators) here
        CounterActions,
    ]
})
export class StoreModule {}
