import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';
import { AppState } from '../index';

import * as ActionTypes from './counter.action-types';

@Injectable()
export class CounterActions {

    constructor(
        private ngRedux: NgRedux<AppState>
    ) {}

    public increment (factor) {
        this.ngRedux.dispatch({
            type: ActionTypes.COUNTER_INCREMENT,
            factor,
        });
    }

    public decrement (factor) {
        this.ngRedux.dispatch({
            type: ActionTypes.COUNTER_DECREMENT,
            factor,
        });
    }
}
