import { CounterStateInterface } from './counter.types';
import * as actionTypes from './counter.action-types';
import { counterInitialState } from './counter.initial-state';

export const counterReducer = (state: CounterStateInterface = counterInitialState, action: any) => {
    switch (action.type) {

        case actionTypes.COUNTER_INCREMENT:
            return {
                previous: state.value,
                value: state.value + action.factor
            };

        case actionTypes.COUNTER_DECREMENT:
            return {
                previous: state.value,
                value: state.value - action.factor
            };

        default:
            return state;
    }
}
