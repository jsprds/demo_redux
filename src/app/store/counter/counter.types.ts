export interface CounterStateInterface {
    previous: number;
    value: number;
};
