import { HomePage } from './home';
import { NoContentPage } from './no-content';

export const Pages = [
    HomePage,
    NoContentPage
];
