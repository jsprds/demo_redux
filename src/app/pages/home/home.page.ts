import {
    Component,
    OnInit
} from '@angular/core';

import { select } from '@angular-redux/store';

import { CounterActions } from '../../store/counter';

@Component({
    selector: 'home-page',
    styleUrls: ['./home.page.scss'],
    templateUrl: './home.page.html'
})
export class HomePage implements OnInit {
    @select(['counter']) public counter$;

    constructor(private counterActions: CounterActions) {}

    public ngOnInit() {
        console.log('HomePage initialized');
    }

    public increment(): void {
        this.counterActions.increment(1);
    }

    public decrement(): void {
        this.counterActions.decrement(1);
    }
}
