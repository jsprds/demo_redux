import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store';

import { AppComponent } from './app.component';
import { Pages } from './pages';
import { Components } from './components';
import { AuiModules } from './aui.modules';

import { AppRoutingModule } from './app-routing.module';

import { initialState, StoreModule } from './store';
import { AppState, rootReducer } from './store';

const DEVMODE = process.env.NODE_ENV === 'DEV';


@NgModule({
    imports: [
        BrowserModule,
        NgReduxModule,
        StoreModule,
        AppRoutingModule,
        ...AuiModules
    ],
    declarations: [
        AppComponent,
        ...Pages,
        ...Components
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(
        public appRef: ApplicationRef,
        private ngRedux: NgRedux<AppState>,
        private devTools: DevToolsExtension
    ) {
        const enhancers = DEVMODE && devTools.isEnabled() ? [ devTools.enhancer() ] : [];

        this.ngRedux.configureStore(rootReducer, initialState, [], enhancers);
    }
}
