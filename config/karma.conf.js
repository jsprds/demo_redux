var webpackConfig = require('../webpack.config.js');

module.exports = function (config) {
    var configuration = {
        basePath: '',

        frameworks: [
            'jasmine'
        ],

        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'),
            require('karma-coverage-istanbul-reporter'),
            require('karma-mocha-reporter'),
            require('karma-webpack')
        ],

        files: [ './config/karma.entry.js' ],

        preprocessors: {
            './config/karma.entry.js': [ 'webpack' ]
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            stats: 'errors-only'
        },

        webpackServer: {
            noInfo: true
        },

        reporters: [
            'mocha',
            'coverage-istanbul',
            'kjhtml'
        ],

        coverageIstanbulReporter: {
            reports: [ 'html', 'lcovonly' ],
            fixWebpackSourcePaths: true
        },

        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: [
            'Chrome'
        ],
        singleRun: true
    };

    config.set(configuration);
};
