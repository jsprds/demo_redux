module.exports = {
    title: 'App app | ACPaaS Angular App generator',
    description: 'App app made with the ACPaaS Angular App generator.',
    baseUrl: '/',
    themeColor: '#CE1A32',
    GTMContainerId: '',
    GATag: ''
};
